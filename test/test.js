const expect = require("chai").expect;
const makeFileHandler = require("../file-handler");
const sinon = require("sinon");

describe("FileHandler", () => {
  let sut;
  let channel;
  let readFile;
  let getExtension;
  let guid;

  beforeEach(() => {
    channel = {};
    readFile = sinon.fake();
    getExtension = sinon.fake.returns("e");
    guid = sinon.fake.returns("g");

    sut = makeFileHandler({
      channel,
      readFile,
      getExtension,
      guid
    });
  });
  describe("onFileUpload", () => {
    it("should extract correct info", () => {
      sut.onFileUpload({ name: "toto", path: "path" });
      expect(guid.calledOnce).to.be.true;
      expect(getExtension.calledWith("toto")).to.be.true;
    });
  });
});
