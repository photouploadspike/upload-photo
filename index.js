#!/usr/bin/env node

const http = require("http");
const formidable = require("formidable");
const amqp = require("amqplib/callback_api");
const makeFileHandler = require("./file-handler");
const uuidv4 = require("uuid/v4");
const path = require("path");
const fs = require("fs");
const retry = require("retry");

const startServer = ({ onFileUpload }) =>
  http
    .createServer((req, res) => {
      if (req.url == "/fileupload") {
        const form = new formidable.IncomingForm();
        form.on("file", (field, file) => {
          onFileUpload(file);
        });
        form.on("end", () => {
          res.writeHead(301, {
            Location: "/"
          });
          res.end();
        });
        form.parse(req);
      } else {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.write(
          '<form action="fileupload" method="post" enctype="multipart/form-data">'
        );
        res.write('<input type="file" name="upload"  multiple="multiple"><br>');
        res.write('<input type="submit">');
        res.write("</form>");
        return res.end();
      }
    })
    .listen(8080);

const getAmqpHost = () => {
  return process.env.AMQP_HOST ? process.env.AMQP_HOST : "localhost";
};

const connectWithRetry = (connectionString, callback) => {
  const operation = retry.operation({
    retries: 5,
    factor: 3,
    minTimeout: 1 * 1000,
    maxTimeout: 60 * 1000,
    randomize: true
  });
  operation.attempt(currentAttempt => {
    amqp.connect(connectionString, (err, conn) => {
      if (operation.retry(err)) {
        return;
      }

      callback(err ? operation.mainError() : null, conn);
    });
  });
};

connectWithRetry("amqp://" + getAmqpHost(), (err, conn) => {
  if (err) throw { msg: "could not connect to rmq" };
  conn.createChannel(function(err, ch) {
    ch.assertExchange("image", "topic", { durable: false }, () => {
      startServer(
        makeFileHandler({
          channel: ch,
          readFile: fs.readFile,
          getExtension: path.extname,
          guid: uuidv4
        })
      );
    });
  });
});
