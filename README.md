# Goals

Allow a user to upload photos

# Run

## Middleware

Before running the application, make sure rabbitMq is running and available.
You can change rabbit mq HOST with the environment variable AMQP_HOST.

If the variable is not define, the app falls back to localhost.

## Launch

With node and npm you can :
```sh
npm i
npm start
```

With docker you can 
```sh
docker build -t upload-photo .
docker run -d  -e AMQP_HOST=10.0.0.1 -p 8080:8080 upload-photo:latest
```