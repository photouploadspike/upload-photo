const makeFileHandler = ({ channel, readFile, getExtension, guid }) => ({
  channel: channel,
  onFileUpload: file => {
    const imageId = guid();
    const extension = getExtension(file.name);
    const tempPath = file.path;
    const newName = imageId + extension;

    readFile(tempPath, {}, (err, buffer) => {
      channel.publish("image", "image-upload", buffer, {
        headers: { imageId: imageId, filename: newName }
      });
    });
  }
});

module.exports = makeFileHandler;
